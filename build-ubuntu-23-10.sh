#!/bin/sh
docker buildx build --platform linux/armhf -t jkridner/ubuntu-23-10-arm --push -f Dockerfile.arm .
docker buildx build --platform linux/amd64 -t jkridner/ubuntu-23-10-amd64 --push -f Dockerfile.amd64 .
