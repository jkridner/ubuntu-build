FROM jkridner/ubuntu-23-10-$TARGETARCH:latest as ubuntu-build
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get install -y apt-utils \
    && apt-get clean
RUN apt-get update \
    && apt-get install -y locales \
	&& localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
    && apt-get clean
ENV LANG en_US.utf8
RUN apt-get update \
    && apt-get install -y \
        build-essential curl gpg git wget tar \
        autoconf libtool autotools-dev \
        libssl-dev libxml2-dev libyaml-dev libgmp-dev libz-dev \
        file cpio unzip bc rsync bison flex device-tree-compiler python3-distutils swig python3-dev ccache \
    && apt-get clean
RUN apt-get update \
    && apt-get install -y \
        devscripts \
    && apt-get clean
